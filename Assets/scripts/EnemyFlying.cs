﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFlying : MonoBehaviour
{
    public ParticleSystem explosion;

    Vector3 finalVector;
    // Use this for initialization
    private enum STATE
    {
        MOVE,
        IDLE
    }

    private STATE state = STATE.MOVE;

    private void SetState(STATE state)
    {
        this.state = state;
    }

    void Start()
    {
        finalVector = new Vector3(transform.position.x - 100, 4, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (state == STATE.MOVE)
        {
            MovePhase();
        }
    }

    void MovePhase()
    {
        this.transform.position = Vector3.MoveTowards(transform.position, finalVector, 10f * Time.deltaTime);
        if (transform.position == finalVector)
        {
            SetState(STATE.IDLE);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("bullet"))
        {
            Debug.Log("collided with bullet");
            DeathStart();
        }

    }


    public void DeathStart()
    {
        CoreController.Instance.AudioSelector(3);
        DestroyObject(this.gameObject);
    }

}
