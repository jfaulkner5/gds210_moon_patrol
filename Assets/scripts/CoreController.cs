﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DefaultExecutionOrder(-100)]
public class CoreController : MonoBehaviour
{


    private static CoreController _coreController;
    public static CoreController Instance
    {
        get
        {
            if (_coreController == null)
            {
                _coreController = FindObjectOfType<CoreController>();

                if (_coreController == null)
                {
                    Debug.LogError("no core");
                }
            }
            return _coreController;

        }
    }


    public GameObject deathParticle;
    public List<AudioClip> Soundbank;
    public AudioClip clipToPlay;
    [SerializeField]
    public AudioSource aud;

    private void Awake()
    {
        if (_coreController == null)
        {
            _coreController = this;
        }
        else
        {
            DestroyImmediate(gameObject);
        }
    }


    public void ParticleExplosion(Transform spawnLoc)
    {
        Instantiate(deathParticle, spawnLoc);
    }

    //public void AudioSelector(string clipToPlay)
    //{
    //    foreach (AudioClip clip in Soundbank)
    //    {
    //        if (clip.name == clipToPlay)
    //            aud.PlayOneShot(clip);
    //    }
    //}

    public void AudioSelector(int clipIndex)
    {
        clipToPlay = Soundbank[clipIndex];
        aud.PlayOneShot(clipToPlay);
    }

}
