﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class BulletController : MonoBehaviour
{
    public GameObject explosion;
    public float decayTime = 3;
    public float bulletSpeed = 20;

    [SerializeField] private Rigidbody _bulletRB;
    public Rigidbody BulletRB
    {
        get
        {
            if (_bulletRB == null)
            {
                _bulletRB = GetComponent<Rigidbody>();

                if (_bulletRB == null)
                {
                    Debug.LogError("BulletController could not find attached rigidbody and will not function.");
                }
            }

            return _bulletRB;
        }
    }

    public void Initialise(Vector3 dir)
    {
        BulletRB.AddForce(dir.normalized * bulletSpeed, ForceMode.VelocityChange);
        Destroy(gameObject, decayTime);
    }

    public void OnDestroy()
    {
        Transform finalLoc = this.transform;
        CoreController.Instance.ParticleExplosion(finalLoc);
    }

    public void Start()
    {

    }
}

