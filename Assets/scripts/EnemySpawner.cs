﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    //Enemy prefabs
    // TODO move to a list? 
    public GameObject enemyBasic, enemyFlying, selectedPrefab;

    // spawn location areas
    public Transform basicSpawn, flySpawn;
    public bool isBasicClear, isFlyClear;

    //instantiated enemy related stuff
    public GameObject basicIns, flyingIns;
    public GameObject enemyParent; //used to parent them to the enemy folder for ease

    // counter variables
    public float spawnCounter;
    public float currentCount;
    public bool isStartCount;


    // Use this for initialization
    void Start()
    {
        // HACK not functionally finding the spawn loc objects. overridden by inspector for now

        //basicSpawn = GameObject.Find("SpawnLocOne");
        //flySpawn = GameObject.Find("SpawnLocTwo");

        isStartCount = true;
        spawnCounter = 5;

    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (Input.GetKey(KeyCode.Alpha1))
        {
            DebugSpawn(basicSpawn, enemyBasic);
        }
        if (Input.GetKey(KeyCode.Alpha2))
        {
            DebugSpawn(flySpawn, enemyFlying);
        }

        if (isStartCount)
        {
            StartCoroutine(WaitTimer());
            Debug.Log("coroutine started");
        }
    }

    private IEnumerator DebugSpawn(Transform pos, GameObject prefab)
    {
        SpawnCheck(pos, prefab);
        yield return new WaitForSecondsRealtime(1);
    }

    private IEnumerator WaitTimer()
    {
        isStartCount = !isStartCount;
        Debug.Log("Co routine wait timer has being started. waiting for up to " + spawnCounter + " seconds");
        yield return new WaitForSecondsRealtime(5);
        SpawnCounter();
        isStartCount = !isStartCount;
    }

    void SpawnCounter()
    {
        Debug.Log("spawn stuff should be initiated here");

        //simple randomiser to select who to spawn
        selectedPrefab = SpawnChooser();

        //FIX am I able to fix the SpawnChooser() to remove this IF
        if (selectedPrefab == enemyBasic)
        {
            SpawnCheck(basicSpawn, selectedPrefab);
        }
        else
        {
            SpawnCheck(flySpawn, selectedPrefab);
        }
        //FIX COUNTER RESET :: Reset() should work
        //currentCount = spawnCounter;
    }

    public void SpawnCheck(Transform spawnLoc, GameObject prefabToSpawn)
    {
        Debug.Log("spawn is called");
        //grab spawnloc
        Vector3 down = new Vector3(0, -1);

        Ray spawnCheckRay = new Ray(spawnLoc.position, down);
        RaycastHit hitData;
        if (Physics.Raycast(spawnCheckRay, out hitData))
        {
            if (hitData.collider.tag == "ground")
            {
                SpawnEnemy(hitData.point, prefabToSpawn);
            }

        }
    }

    void SpawnEnemy(Vector3 spawnLoc, GameObject prefabToSpawn)
    {
        if (Random.value <= 0.5f)
        {
            CoreController.Instance.AudioSelector(4);
            GameObject prefabIns = Instantiate(prefabToSpawn, spawnLoc, this.transform.rotation);
            //prefabIns.transform.parent = enemyParent.transform;
        }
    }

    public GameObject SpawnChooser()
    {
        GameObject chosenPrefab;
        if (Random.value <= 0.5)
        {
            chosenPrefab = enemyBasic;
        }
        else
        {
            chosenPrefab = enemyFlying;
        }

        return chosenPrefab;
    }
}
