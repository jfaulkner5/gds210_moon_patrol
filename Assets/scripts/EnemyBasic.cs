﻿using UnityEngine;


public class EnemyBasic : MonoBehaviour
{

    private void OnTriggerEnter(Collider other)
    {

        if (other.CompareTag("bullet"))
        {
            Debug.Log("hit by bullet");
            DeathStart();


            DestroyObject(other.gameObject);
        }

        else if (other.CompareTag("Player"))
        {
            Debug.Log("collided with player");

            DeathStart();
        }

    }

    public void HitBullet(GameObject other)
    {

    }
    public void DeathStart()
    {   DestroyObject(gameObject);
        CoreController.Instance.AudioSelector(3);
    }

}
