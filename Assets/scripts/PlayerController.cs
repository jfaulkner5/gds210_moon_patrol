﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{

    public Rigidbody playerRB;
    public Transform playerTransform;
    public GameObject explosion;
    public float playerSpeed; //velocity
    public float moveX;
    public float jumpPower;

    public Vector3 initialPosition;
    public bool isOnGround;
    public bool isFireReady;
    public float attackCounter;
    public float attackCounterMax;

    public Rigidbody bulletPrefab;
    private Rigidbody bulletHor, bulletVir;
    private Vector3 spawnPosition;
    private Vector3 spawnOffest; //offest for the bullet spawn
    //FIX; doesn't need to be a GO, can just be Trans
    public GameObject bulletSpawnOne, bulletSpawnTwo;

    private float bulletSpeed;

    //player controls
    public string leftKey, rightKey, jumpkey, attackKey, attackTwoKey, menuKey;

    // enemy collision related junk
    public GameObject enemy;



    // Use this for initialization
    void Start()
    {

        bulletSpawnOne = GameObject.Find("BulletSpawn");

        playerRB = this.GetComponent<Rigidbody>();
        playerTransform = this.transform;
        playerSpeed = 5.0f;
        jumpPower = 1.5f;
        initialPosition = this.transform.position;
        isOnGround = false;
        attackCounter = 0;
        attackCounterMax = 1;


        // set player controls
        leftKey = "a";
        rightKey = "d";
        jumpkey = "w";
        attackKey = "space";

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKey(leftKey))
        {
            moveX = Input.GetAxis("Horizontal");
            playerRB.velocity = new Vector3(moveX * playerSpeed, playerRB.velocity.y);
        }

        if (Input.GetKey(rightKey))
        {
            moveX = Input.GetAxis("Horizontal");
            playerRB.velocity = new Vector3(moveX * playerSpeed, playerRB.velocity.y);
        }

        if (Input.GetKey(jumpkey))
        {
            if (isOnGround)
            {
                Debug.Log("jump was pressed && isOnGround == true");
                playerRB.velocity = new Vector3(playerRB.velocity.x, jumpPower * playerSpeed);
            }
        }



        Killbox();
        AttackPrimary();

    }

    void Movement()
    {

    }



    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("ground"))
        {
            isOnGround = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("enemy"))
        {
            Debug.Log("collision with enemy");
            Destroy(other.gameObject);
            Death();

        }
    }


    private void OnCollisionExit(Collision collision)
    {
        if (collision.collider.CompareTag("ground"))
        {
            isOnGround = false;
        }
    }

    public void AttackPrimary()
    {
      

        if (attackCounter <= 0)
        {
            if (Input.GetKey(attackKey))
            {
                CoreController.Instance.AudioSelector(1);
                Debug.Log("Space bar was pressed");
                bool isHor = true;
                SpawnBullet(bulletSpawnOne.transform, isHor);
                isHor = false;
                SpawnBullet(bulletSpawnTwo.transform, isHor);

                attackCounter = attackCounterMax;
            }
        }
        else
        {

            attackCounter -= Time.deltaTime;
        }

    }

    public void SpawnBullet(Transform spawnFrom, bool isHorizontal)
    {
        var newBullet = Instantiate(bulletPrefab, spawnFrom.position, spawnFrom.rotation) as Rigidbody;
        var bscont = newBullet.GetComponent<BulletController>();

        if (isHorizontal)
        {
            bscont.Initialise(spawnFrom.forward);
        }
        else
        {
            bscont.Initialise(spawnFrom.up);
        }
    }

    public void Death()
    {
        this.transform.position = initialPosition;
        Instantiate(explosion, this.transform);
        CoreController.Instance.AudioSelector(3);

        //play explosion
        // Reset game. display score
    }

    void Killbox()
    {
        if (transform.position.y <= -10)
        {
            Death();
        }
    }
}
